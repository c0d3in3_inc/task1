package com.c0d3in3.task1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val usersList = mutableListOf<User>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        val email = emailEditText.text
        val age = ageEditText.text
        val firstName = firstNameEditText.text
        val lastName = lastNameEditText.text

        addUser.setOnClickListener {
            if(email.isEmpty() || age.isEmpty() || firstName.isEmpty() || lastName.isEmpty()) return@setOnClickListener Toast.makeText(this, "Fill all the fields!", Toast.LENGTH_SHORT).show()
            if(age.toString().length > 3 || age.toString().toInt() <= 0) return@setOnClickListener Toast.makeText(this, "Invalid age format!", Toast.LENGTH_SHORT).show()
            if(!isEmailValid(email.toString())) return@setOnClickListener Toast.makeText(this, "Invalid e-mail format!", Toast.LENGTH_SHORT).show()
            if (checkUser(firstName.toString(), lastName.toString(), age.toString().toInt(), email.toString())) {
                Toast.makeText(this, "User already exists", Toast.LENGTH_SHORT).show()
            }
            else {
                usersList.add(User(firstName.toString(), lastName.toString(), age.toString().toInt(), email.toString()))
                Toast.makeText(this, "User added successfully", Toast.LENGTH_SHORT).show()
                println(User(firstName.toString(), lastName.toString(), age.toString().toInt(), email.toString()))
            }
        }

        removeUser.setOnClickListener {
            if(email.isEmpty() || age.isEmpty() || firstName.isEmpty() || lastName.isEmpty()) return@setOnClickListener Toast.makeText(this, "Fill all the fields!", Toast.LENGTH_SHORT).show()
            if(age.toString().length > 3 || age.toString().toInt() <= 0) return@setOnClickListener Toast.makeText(this, "Invalid age format!", Toast.LENGTH_SHORT).show()
            if(!isEmailValid(email.toString())) return@setOnClickListener Toast.makeText(this, "Invalid e-mail format!", Toast.LENGTH_SHORT).show()
            if (checkUser(firstName.toString(), lastName.toString(), age.toString().toInt(), email.toString())) {
                removeUser(firstName.toString(), lastName.toString(), age.toString().toInt(), email.toString())
                Toast.makeText(this, "User deleted successfully", Toast.LENGTH_SHORT).show()
            }
            else{
                Toast.makeText(this, "User does not exits", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun checkUser(firstName: String, lastName: String, age: Int, email: String): Boolean {
        usersList.forEach{
            if(it.firstName == firstName && it.lastName == lastName && it.age == age && it.email == email) return true
        }
        return false
    }

    private fun removeUser(firstName: String, lastName: String, age: Int, email: String){
        for(user in usersList){
            if(user.firstName == firstName && user.lastName == lastName && user.age == age && user.email == email){
                usersList.remove(user)
            }
        }
    }

    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}
